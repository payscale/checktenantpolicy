using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;

namespace CheckTenantPolicy
{
    /// <summary>
    /// To run this function locally, please add the following to your environment variables
    /// 'tenantId'(int) => Tp Test account's tenantId
    /// 'shard0' (string) => connection string to your local DB
    /// PLEASE RESTART VS to reflect the ^ newly added variables
    /// </summary>
    public class CheckPolicy
    {
        [FunctionName(nameof(CheckPolicy))]
        public async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Anonymous)]
            HttpRequestMessage req, ILogger log, ExecutionContext context)
        {
            log.LogInformation($"{nameof(CheckPolicy)} function executed at: {DateTime.Now}");

            var result = new PolicyCheckResults();

            // Reads connectionstring and tenantId from App Config settings of function app.
            var dbConnectionString = Environment.GetEnvironmentVariable("shard0");
            var tenantId = Environment.GetEnvironmentVariable("tenantId");
            if (int.TryParse(tenantId, out _))
            {
                await using (SqlConnection conn = new SqlConnection(dbConnectionString))
                {
                    conn.Open();
                    var query = "Exec Employee.CheckTenantPolicy @TenantId";

                    await using (SqlCommand empCmd = new SqlCommand(query, conn))
                    {
                        empCmd.Parameters.Add("@TenantId", SqlDbType.NVarChar).Value = tenantId;
                        result.NumberOfActiveTenantIdInEmployee = (int) await empCmd.ExecuteScalarAsync();
                    }

                    query = "Exec Team.CheckTenantPolicy @TenantId";
                    await using (SqlCommand teamCmd = new SqlCommand(query, conn))
                    {
                        teamCmd.Parameters.Add("@TenantId", SqlDbType.NVarChar).Value = tenantId;
                        result.NumberOfActiveTenantIdTeam = (int) await teamCmd.ExecuteScalarAsync();
                    }
                }

                log.LogInformation(
                    $"{DateTime.Now}: Employee {result.NumberOfActiveTenantIdInEmployee} and Team {result.NumberOfActiveTenantIdTeam} tenantIds active!");
                if (result.NumberOfActiveTenantIdInEmployee == 1 && result.NumberOfActiveTenantIdTeam == 1)
                {
                    return new OkObjectResult(result);
                }

                return new ExceptionResult(new Exception("Either Team or Employee Tenant Policy is OFF!"), true);
            }

            return new BadRequestErrorMessageResult(
                "Please ensure the tenantId variable in app settings has a value set");
        }

        public class PolicyCheckResults
        {
            public int NumberOfActiveTenantIdInEmployee { get; set; }
            public int NumberOfActiveTenantIdTeam { get; set; }
        }
    }
}
